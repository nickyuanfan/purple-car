package app;

import com.sun.tools.corba.se.idl.InvalidArgument;

import ch.qos.logback.core.encoder.EchoEncoder;

public class CarValidator {

  public void validate(Car car) throws Exception {
      Car carObj = (Car) car;
      notNull(carObj);
      validateCarId(carObj.getcarID());
      validateCarVin(carObj.getvin());
      validateCarModel(carObj.getmodel());
 }

    private void validateCarModel(String getmodel) throws Exception {
      if(getmodel == null || getmodel.isEmpty()) {
          throw new NullPointerException("Car model is null");
      }
      if(!getmodel.matches("[0-9X]*")){
          throw new Exception("Characters much be between 0-9 and Alphabet letters");
      }
    }

    private void validateCarVin(String getvin) throws Exception {
        if(getvin == null || getvin.isEmpty()) {
            throw new NullPointerException("Car model is null");
        }
        else if(!getvin.matches("[0-9X]*")){
            throw new Exception("Characters much be between 0-9 and Alphabet letters");
        }
    }

    private void notNull(Car carObj) {
        if(carObj == null) {
            throw new NullPointerException("Car model is null");
        }
    }

    private void validateCarId(Integer getcarID) {
      if(getcarID < 0) {
          throw new ArithmeticException("Illegal carID value");
      }
      else if(Double.isInfinite(getcarID) || Double.isNaN(getcarID)) {
          throw new ArithmeticException("illegal CarID value: ");
      }
      else if(!betweenExclusive(getcarID,0,100)) {
          throw new ArithmeticException("illegal CarID value: ");
      }
    }

    public static boolean betweenExclusive(int x, int min, int max)
    {
        return x>min && x<max;
    }


}
